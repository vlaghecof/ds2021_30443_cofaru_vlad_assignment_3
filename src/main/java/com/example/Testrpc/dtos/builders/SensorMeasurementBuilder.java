package com.example.Testrpc.dtos.builders;


import com.example.Testrpc.dtos.SensorMeasurementDTO;
import com.example.Testrpc.entities.Sensor;
import com.example.Testrpc.entities.SensorMeasurement;
import lombok.NoArgsConstructor;

/**
 * The class  SensorMeasurementBuilder
 *
 * @author Cofaru Vlad
 */
@NoArgsConstructor
public class SensorMeasurementBuilder {

    public static SensorMeasurementDTO toDTO(SensorMeasurement sensorMeasurement) {
        return SensorMeasurementDTO.builder()
                .id(sensorMeasurement.getId())
                .energyConsumption(sensorMeasurement.getEnergyConsumption())
                .timestamp(sensorMeasurement.getTimestamp())
                .sensorID(sensorMeasurement.getSensor().getId())
                .build();
    }

    public static SensorMeasurement toEntity(SensorMeasurementDTO sensorMeasurementDTO, Sensor sensor) {
        return SensorMeasurement.builder()
                .id(sensorMeasurementDTO.getId())
                .energyConsumption(sensorMeasurementDTO.getEnergyConsumption())
                .timestamp(sensorMeasurementDTO.getTimestamp())
                .sensor(sensor)
                .build();
    }
}
