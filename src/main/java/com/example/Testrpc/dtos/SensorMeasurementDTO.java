package com.example.Testrpc.dtos;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * The class  SensorMeasurementDTO
 *
 * @author Cofaru Vlad
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class SensorMeasurementDTO extends RepresentationModel<SensorMeasurementDTO> {

    private UUID id;

    private LocalDateTime timestamp;

    private Integer energyConsumption;

    private UUID sensorID;

    @Override
    public String toString() {
        return "SensorMeasurementDTO{" +
                "timestamp=" + timestamp +
                ", energyConsumption=" + energyConsumption +
                ", Id=" + sensorID +
                '}';
    }
}
