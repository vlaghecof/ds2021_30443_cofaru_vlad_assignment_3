package com.example.Testrpc.service;

/**
 * The class  MeasurementService
 *
 * @author Cofaru Vlad
 */

import com.example.Testrpc.dtos.SensorMeasurementDTO;
import com.example.Testrpc.dtos.builders.SensorMeasurementBuilder;
import com.example.Testrpc.entities.Sensor;
import com.example.Testrpc.entities.SensorMeasurement;
import com.example.Testrpc.repositories.AccountRepository;
import com.example.Testrpc.repositories.DeviceRepository;
import com.example.Testrpc.repositories.SensorMeasurementRepository;
import com.example.Testrpc.repositories.SensorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class MeasurementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeasurementService.class);
    private final AccountRepository accountRepository;
    private final DeviceRepository deviceRepository;
    private final SensorRepository sensorRepository;
    private final SensorMeasurementRepository sensorMeasurementRepository;

    public MeasurementService(AccountRepository accountRepository, DeviceRepository deviceRepository, SensorRepository sensorRepository, SensorMeasurementRepository measurementRepository) {
        this.accountRepository = accountRepository;
        this.deviceRepository = deviceRepository;
        this.sensorRepository = sensorRepository;
        this.sensorMeasurementRepository = measurementRepository;
    }


    public List<SensorMeasurementDTO> findSensorMeasurement() {
        List<SensorMeasurement> sensorMeasurements = sensorMeasurementRepository.findAll();
        LOGGER.info("AM GASIT O " +sensorMeasurements  );
        return sensorMeasurements.stream()
                .map(SensorMeasurementBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public List<SensorMeasurementDTO> findSensorMeasurementByDeviceId(UUID id) {
        List<SensorMeasurement> sensorMeasurements = sensorMeasurementRepository.findBySensor_SensorDevice_Id(id);
        return sensorMeasurements.stream()
                .map(SensorMeasurementBuilder::toDTO)
                .collect(Collectors.toList());
    }


    public List<Integer> computeBaseline(UUID id) {
        List<SensorMeasurement> sensorMeasurements = sensorMeasurementRepository.findBySensor_SensorDevice_Id(id);
        LocalDate currentDate = LocalDate.now();
        List<SensorMeasurement> filteredMeasurements = sensorMeasurements.stream().filter(measurement -> measurement.getTimestamp().toLocalDate().isAfter(currentDate.minusDays(7))).collect(Collectors.toList());

        List<Integer> valuesPerHour =new ArrayList<Integer>(Collections.nCopies(25, 0)); // to store the values from 0 to 23

        for(SensorMeasurement currentRead : filteredMeasurements)
        {
            Integer currentValue = valuesPerHour.get(currentRead.getTimestamp().getHour());
            valuesPerHour.set(currentRead.getTimestamp().getHour(),currentValue + currentRead.getEnergyConsumption());
        }
        valuesPerHour = valuesPerHour.stream().map(x -> x/7).collect(Collectors.toList());

        return valuesPerHour;
    }



    public UUID insertWithSensorId(SensorMeasurementDTO sensorMeasurementDTO , UUID sensorID) {
        Sensor sensor  = sensorRepository.findById(sensorID).get();
        SensorMeasurement sensorMeasurement = SensorMeasurementBuilder.toEntity(sensorMeasurementDTO,sensor);
        sensorMeasurement = sensorMeasurementRepository.save(sensorMeasurement);
        return sensorMeasurement.getId();
    }

    public void  doNothing()
    {
        System.out.println("nothing");
    }


}
