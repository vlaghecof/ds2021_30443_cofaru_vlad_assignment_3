package com.example.Testrpc.rpc;
import com.example.Testrpc.dtos.SensorMeasurementDTO;
import com.example.Testrpc.entities.SensorMeasurement;
import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

/**
 * The class  ServerApiInterface
 *
 * @author Cofaru Vlad
 */
@JsonRpcService("/rpc/measurement")
public interface ServerApiInterface {
    int multiplier(@JsonRpcParam(value = "a") int a, @JsonRpcParam(value = "b") int b);

    ResponseEntity<List<SensorMeasurementDTO>> getUserConsumption(@JsonRpcParam(value = "userId")UUID uuid);

    ResponseEntity<List<SensorMeasurementDTO>> getAllMeasurements(@JsonRpcParam(value = "userId")UUID uuid);

    ResponseEntity<UUID> generateMeasurementsForLastWeek(@JsonRpcParam(value = "sensorId") UUID sensorId);

    ResponseEntity<List<SensorMeasurementDTO>> computeBaseline(@JsonRpcParam(value = "sensorId")UUID uuid);

    ResponseEntity<List<SensorMeasurementDTO>> computeStartingTime(@JsonRpcParam(value = "sensorId")UUID uuid);

    ResponseEntity<Integer> getBestTimeToStart(@JsonRpcParam(value = "sensorId")UUID uuid ,@JsonRpcParam(value = "duration") int duration );

}

