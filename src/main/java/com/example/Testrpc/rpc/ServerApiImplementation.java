package com.example.Testrpc.rpc;

/**
 * The class  ServerApiImplementation
 *
 * @author Cofaru Vlad
 */

import com.example.Testrpc.dtos.SensorMeasurementDTO;
import com.example.Testrpc.entities.Device;
import com.example.Testrpc.entities.Sensor;
import com.example.Testrpc.repositories.DeviceRepository;
import com.example.Testrpc.repositories.SensorRepository;
import com.example.Testrpc.service.MeasurementService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AutoJsonRpcServiceImpl
public class ServerApiImplementation implements ServerApiInterface {

    private final MeasurementService measurementService;
    private final SensorRepository sensorRepository;
    private final DeviceRepository deviceRepository;

    public ServerApiImplementation(MeasurementService measurementService, SensorRepository sensorRepository, DeviceRepository deviceRepository) {
        this.measurementService = measurementService;
        this.sensorRepository = sensorRepository;
        this.deviceRepository = deviceRepository;
    }


    @Override
    public int multiplier(int a, int b) {
        return a * b;
    }

    @Override
    public ResponseEntity<List<SensorMeasurementDTO>> getUserConsumption(UUID uuid) {
        return new ResponseEntity<>(measurementService.findSensorMeasurementByDeviceId(uuid), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<SensorMeasurementDTO>> getAllMeasurements(UUID uuid) {
        return new ResponseEntity<>(measurementService.findSensorMeasurement(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UUID> generateMeasurementsForLastWeek(UUID sensorId) {
        List<Sensor> sensors = sensorRepository.findAll();
        List<Device> devices = deviceRepository.findAll();
        LocalDateTime lt = LocalDateTime.now();
//        System.out.println("Am chemat asta");
        Random rand = new Random();
        for (int day = 0; day < 7; day++) {
            for (int i = 1; i < 25; i++) {
                int consumption = rand.nextInt(100);
                SensorMeasurementDTO sensorMeasurementDTO = SensorMeasurementDTO.builder().timestamp(lt.minusDays(day).minusHours(i)).energyConsumption(consumption).build();
                measurementService.insertWithSensorId(sensorMeasurementDTO, sensorId);
//                System.out.println(sensorMeasurementDTO);
            }
        }

        return new ResponseEntity<>(sensorId, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<SensorMeasurementDTO>> computeBaseline(UUID uuid) {
        List<Integer> valuePerHours = measurementService.computeBaseline(uuid);
        List<SensorMeasurementDTO> baseline = new LinkedList<>();
        for (int i = 0; i < 24; i++) {
            LocalDateTime lt = LocalDateTime.now().withHour(i);
            SensorMeasurementDTO sensorMeasurementDTO = SensorMeasurementDTO.builder().timestamp(lt).energyConsumption(valuePerHours.get(i)).build();

            baseline.add(sensorMeasurementDTO);
        }

        return new ResponseEntity<>(baseline, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<SensorMeasurementDTO>> computeStartingTime(UUID uuid) {
        Device device = deviceRepository.findById(uuid).get();
        List<Integer> valuePerHours = measurementService.computeBaseline(uuid);
        List<SensorMeasurementDTO> baseline = new LinkedList<>();
        for (int i = 0; i < 24; i++) {
            LocalDateTime lt = LocalDateTime.now().withHour(i);
            SensorMeasurementDTO sensorMeasurementDTO = SensorMeasurementDTO.builder().timestamp(lt).energyConsumption(valuePerHours.get(i) + device.getMaximumConsumption()).build();

            baseline.add(sensorMeasurementDTO);
        }

        return new ResponseEntity<>(baseline, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Integer> getBestTimeToStart(UUID uuid, int duration) {
        Device device = deviceRepository.findById(uuid).get();
        List<Integer> valuePerHours = measurementService.computeBaseline(uuid);

        valuePerHours = valuePerHours.stream().map(e -> e + device.getMaximumConsumption()).collect(Collectors.toList());
        List<Integer> maximumValues = new LinkedList<>();
        int minDuration=4444444;
        int timeStart=-33;
        for (int i=0; i<24-duration;i++)
        {
            int maxValue=0;
            for(int j=0;j<duration;j++)
            {
               maxValue=Math.max (valuePerHours.get(i + j),maxValue);
            }
            if(maxValue<minDuration)
            {
                timeStart=i;
                minDuration=maxValue;
            }
            maximumValues.add(maxValue);

        }
        System.out.println(timeStart);

        return new ResponseEntity<>(timeStart, HttpStatus.OK);
    }


}
