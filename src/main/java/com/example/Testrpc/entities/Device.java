package com.example.Testrpc.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;


@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "max_consumption", nullable = false)
    private Integer maximumConsumption;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "average_consumption", nullable = false)
    private Integer averageConsumption;

    @ManyToOne
    @JoinColumn(name="account_id", nullable = false)
    private Account account;

    @OneToOne(mappedBy = "sensorDevice",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Sensor monitoringSensor;

    @Override
    public String toString() {
        return description  +" - "+ location ;
    }
}