package com.example.Testrpc.repositories;


import com.example.Testrpc.entities.SensorMeasurement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface SensorMeasurementRepository extends JpaRepository<SensorMeasurement, UUID> {

    List<SensorMeasurement> findBySensor_Id(UUID sensorID);


    List<SensorMeasurement> findBySensor_SensorDevice_Id(UUID deviceId);




}
