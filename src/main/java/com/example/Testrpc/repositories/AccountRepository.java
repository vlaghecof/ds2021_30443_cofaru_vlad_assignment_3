package com.example.Testrpc.repositories;


import com.example.Testrpc.entities.Account;
import com.example.Testrpc.entities.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {

    Account findByName(String name);


    List<Account> getAccountByDeviceListContaining(Device device);

    Account findFirstByDeviceListContaining(Device device);


}
