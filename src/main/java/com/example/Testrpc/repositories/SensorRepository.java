package com.example.Testrpc.repositories;


import com.example.Testrpc.entities.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface SensorRepository extends JpaRepository<Sensor, UUID> {

    List<Sensor> findBySensorDevice_Account_Id(UUID accountId);
}
