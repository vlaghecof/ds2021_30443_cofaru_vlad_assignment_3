package com.example.Testrpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestrpcApplication {

	public static void main(String[] args) {

		SpringApplication.run(TestrpcApplication.class, args);
	}

}
